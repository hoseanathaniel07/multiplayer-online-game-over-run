﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class CanvasHandler : MonoBehaviour
{
    public static CanvasHandler instance;

    [Header("Login")]
    public GameObject loginView;
    public TMP_InputField userLoginInput;
    public TMP_InputField passLoginInput;
    public Button logSubmit;
    public Button viewToReg;

    [Header("Register")]
    public GameObject registerView;
    public TMP_InputField userRegInput;
    public TMP_InputField passRegInput;
    public Button regSubmit;
    public Button viewToLog;

    public TextMeshProUGUI msgText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    private void Start()
    {
        loginView.SetActive(false);
        registerView.SetActive(true);

        viewToReg.onClick.AddListener(RegisterViewFunc);
        viewToLog.onClick.AddListener(LoginViewFunc);

        regSubmit.onClick.AddListener(Register);
        logSubmit.onClick.AddListener(Login);
    }

    public void RegisterViewFunc()
    {
        loginView.SetActive(false);
        registerView.SetActive(true);
    }

    public void LoginViewFunc()
    {
        loginView.SetActive(true);
        registerView.SetActive(false);
    }

    public void Register()
    {
        if (userRegInput.text == "" || userRegInput.text.Length == 0)
        {
            SetMessageText("Email is empty");
        }
        else if (passRegInput.text == "" || passRegInput.text.Length == 0)
        {
            SetMessageText("Password is empty");
        }
        else
        {
            ClientSend.Register(userRegInput.text, passRegInput.text);
        }
    }

    public void Login()
    {
        if (userLoginInput.text == "" || userLoginInput.text.Length == 0)
        {
            SetMessageText("Email is empty");
        }
        else if (passLoginInput.text == "" || passLoginInput.text.Length == 0)
        {
            SetMessageText("Password is empty");
        }
        else
        {
            ClientSend.Login(userLoginInput.text, passLoginInput.text);
        }
    }

    public void SetMessageText(string _text)
    {
        msgText.text = _text;
    }

}
