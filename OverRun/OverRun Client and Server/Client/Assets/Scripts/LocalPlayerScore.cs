﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LocalPlayerScore : MonoBehaviour
{
    public Player player;

    private void Start()
    {
        GetComponent<TextMesh>().text = "Your Score - " + player.itemCount;
    }

    private void Update()
    {
        GetComponent<TextMesh>().text = "Your Score - " + player.itemCount;
    }


}
