﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int id;
    public string username;
    public int itemCount = 0;

    public void Initialize(int _id, string _username, bool _isReady, int _textureId)
    {
        id = _id;
        SetUsername(_username);
    }

    public void SetUsername(string _username)
    {
        username = _username;
    }
}
