﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerName : MonoBehaviour
{
    public Player player;



    private void Start()
    {
        GetComponent<TextMesh>().text = player.username + " - " + player.itemCount;
    }

    private void Update()
    {
        GetComponent<TextMesh>().text = player.username + " - " + player.itemCount;
    }
}
