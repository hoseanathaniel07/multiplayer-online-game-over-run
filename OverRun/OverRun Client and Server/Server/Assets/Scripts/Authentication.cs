﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Authentication : MonoBehaviour
{
    public static Authentication instance;
    public List<User> userList = new List<User>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Start()
    {
        User user = new User(3, "hosea@gmail.com", "1234");
        userList.Add(user);
    }

    public void CheckLogin(int _id, string _email, string _password)
    {
        for (int i = 0; i < userList.Count; i++)
        {
            if (_email == userList[i].Email && _password == userList[i].Password)
            {
                ServerSend.SendLoginFeedback(_id, true, "Login Succes");
                return;
            }
        }
        ServerSend.SendLoginFeedback(_id, false, "Login Fail");
    }

    public void Register(int _id, string _email, string _password)
    {
        User _user = new User(_id, _email, _password);
        userList.Add(_user);
        ServerSend.SendRegisterFeedback(_id, true, "Register Success");
    }
}
