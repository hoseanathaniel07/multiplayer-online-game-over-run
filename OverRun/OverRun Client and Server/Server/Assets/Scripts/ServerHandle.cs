﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerHandle
{
    public static void WelcomeReceived(int _fromClient, Packet _packet)
    {
        int _clientIdCheck = _packet.ReadInt();


        Debug.Log($"{Server.clients[_fromClient].tcp.socket.Client.RemoteEndPoint} connected successfully and is now player {_fromClient}.");
        if (_fromClient != _clientIdCheck)
        {
            Debug.Log($"Player (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})!");
        }
        //Server.clients[_fromClient].SendIntoGame(_username);
    }

    public static void PlayerMovement(int _fromClient, Packet _packet)
    {
        bool[] _inputs = new bool[_packet.ReadInt()];
        for (int i = 0; i < _inputs.Length; i++)
        {
            _inputs[i] = _packet.ReadBool();
        }
        Quaternion _rotation = _packet.ReadQuaternion();


        Server.clients[_fromClient].player.SetInput(_inputs, _rotation);
    }

    public static void Login(int _fromClient, Packet _packet)
    {
        string _email = _packet.ReadString();
        string _password = _packet.ReadString();
        Authentication.instance.CheckLogin(_fromClient, _email, _password);
    }

    public static void Register(int _fromClient, Packet _packet)
    {
        string _email = _packet.ReadString();
        string _password = _packet.ReadString();
        Authentication.instance.Register(_fromClient, _email, _password);
    }

    public static void InputUser(int _fromClient, Packet _packet)
    {
        string _username = _packet.ReadString();
        Debug.Log(_username);
        Server.clients[_fromClient].SendIntoGame(_username);
    }
}
