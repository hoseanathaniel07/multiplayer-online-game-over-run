﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    private int userId;
    public int UserId
    {
        get { return userId; }
        private set { userId = value; }
    }

    private string email;
    public string Email
    {
        get { return email; }
        private set { email = value; }
    }

    private string password;
    public string Password
    {
        get { return password; }
        private set { password = value; }
    }

    public User(int _userId, string _email, string _password)
    {
        userId = _userId;
        email = _email;
        password = _password;
    }
}
