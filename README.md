# Multiplayer Online Game - Over Run



**Game Concept**


[Google Slide Concept](https://docs.google.com/presentation/d/1xZeJC92PGlOv_ewytb0dAM8eZlND6Q3Y1gc56vum0DU/edit?usp=sharing)


**Overview**


Over Run merupakan game multiplayer online 3D platformer yang bisa dimainkan oleh 2 player atau lebih, dengan konsep real-time.

Hosted on : Digital Ocean

**Game Flow**

![ALT](/Documentation Pics/Register Page.png "Register page")

Setelah menjalankan aplikasi, player akan masuk ke halaman register, dimana player dapat mendaftarkan email dan password yang akan digunakan untuk akun player


![ALT](/Documentation Pics/Login Page.png "Login page")


Setelah register, player dapat melakukan login pada halaman login menggunakan email dan password yang telah diinput pada halaman register


![ALT](/Documentation Pics/Username Input Page.PNG "Username page")


Setelah login berhasil, player akan langsung dipindahkan ke scene 'game' dan harus memasukkan username yang akan ditampilkan dalam game


![ALT](/Documentation Pics/Main Game scene.PNG "Main Game")


Player akan langsung dispawn kedalam game setelah berhasil mengisikan username, tujuan player adalah mengumpulkan poin yang tersebar di arena permainan, poin berbentuk kristal biru.


**Script Explanation**


- [**Script Server**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/tree/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts)


    - [**Authentication**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/Authentication.cs)
    
    script ini berfungsi untuk melakukan pengecekan ketika user melakukan login dan register


    - [**Client**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/Client.cs)
    
    script ini berfungsi untuk menghubungkan server ke client, spawn player, dan juga mengatur player ketika disconnect


    - [**Constants**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/Constants.cs)
    
    script ini berfungsi untuk menetapkan tick per second dari server, dimana server akan mengirim data ke client sebanyak tick tiap detik


    - [**DontDestroy**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/DontDestroy.cs)
    
    script ini berfungsi agar game object tidak di destroy.


    - [**ItemSpawner**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/ItemSpawner.cs)
    
    script ini mengatur spawn item untuk score dan juga pengambilan item


    - [**NetworkManager**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/NetworkManager.cs)
    
    script ini berfungsi untuk mengatur port, memberhentikan server ketika quit, juga menginstantiate player ketika masuk kedalam game


    - [**Packet**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/Packet.cs)
    
    script ini berfungsi untuk mengatur semua jenis packet yang diterima dan dikirim baik oleh server maupun client, juga berfungsi untuk overload fungsi write dan read data


    - [**Player**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/Player.cs)
    
    script ini berfungsi sebagai penyimpan data player, dimana terdapat id, username, dan score. script ini juga berfungsi untuk menjalankan player pada sisi server dengan menerima input dari player


    - [**SerializeClass**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/SerializeClass.cs)
    
    script ini berfungsi untuk menserialize data login dan register yang dikirim oleh server


    - [**Server**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/Server.cs)
    
    script ini berfungsi untuk menginitialize data dan koneksi tcp dan udp pada server


    - [**ServerHandle**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/ServerHandle.cs)
    
    script ini berfungsi untuk mengatasi semua packet data yang diterima server dari client


    - [**ServerSend**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/ServerSend.cs)
    
    Script ini berfungsi untuk mengatasi semua packet data yang dikirim oleh server ke client


    - [**ThreadManager**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/ThreadManager.cs)
    
    script ini berfungsi untuk mengatasi multi threading yang akan menduplikasi dari main thread


    - [**User**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Server/Assets/Scripts/User.cs)
    
    script ini berfungsi sebagai penyimpan data pengguna: email, password, id



- [**Script Client**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/tree/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts)


    - [**CameraController**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/CameraController.cs)
    
    script ini berfungsi untuk mengatur pergerakan camera dari player


    - [**CanvasHandler**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/CanvasHandler.cs)
    
    script ini berfungsi untuk mengatur tampilan dari login dan register, juga memberi fungsi dari elemen UI pada scene authentication


    - [**Client**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/Client.cs)
    
    script ini berfungsi untuk menghubungkan tcp dan udp client ke server, menginitialize data client, juga menyimpan IP dan port untuk terhubung ke server


    - [**ClientHandle**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/ClientHandle.cs)
    
    script ini berfungsi untuk mengatasi semua data yang diterima oleh client dari server


    - [**ClientSend**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/ClientSend.cs)
    
    script ini berfungsi untuk mengatasi semua data yang akan dikirim oleh client ke server


    - [**DDOL**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/DDOL.cs)
    
    script ini berfungsi agar game object tidak di destroy ketika berpindah scene


    - [**GameManager**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/GameManager.cs)
    
    script ini berfungsi untuk melakukan spawn player dan item, juga memindahkan scene ke game


    - [**ItemSpawner**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/ItemSpawner.cs)
    
    script ini berfungsi untuk menginitialize dan merender item pada sisi client


    - [**LocalPlayerScore**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/LocalPlayerScore.cs)
    
    script ini berfungsi untuk menampilkan score dari player local


    - [**Packet**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/Packet.cs)
    
    script ini berfungsi untuk melist semua packet dari server dan client, juga untuk mengoverwrite write dan read data


    - [**Player**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/Player.cs)
    
    script ini berfungsi untuk menyimpan id, username, dan score dari player


    - [**PlayerController**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/PlayerController.cs)
    
    script ini berfungsi untuk mengirim input dari player ke server


    - [**PlayerName**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/PlayerName.cs)
    
    script ini berfungsi untuk untuk menampilkan username dari player dan score


    - [**SerializeClass**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/SerializeClass.cs)
    
    script ini berfungsi untuk menserialize data login dan register yang akan dikirim


    - [**ThreadManager**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/ThreadManager.cs)
    
    script ini berfungsi sebagai multi threading di sisi client


    - [**UIManager**](https://gitlab.com/hoseanathaniel07/multiplayer-online-game-over-run/-/blob/master/OverRun/OverRun%20Client%20and%20Server/Client/Assets/Scripts/UIManager.cs)
    
    script ini berfungsi untuk mengatur UI yang ada pada scene game
